#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <deque>
//#include "scheduler.h"
//#include <set>


using namespace std;

bool isValidAlg(string alg);
void multilevel(ifstream& inFile);
void realTime(ifstream& inFile);
void windows(ifstream& inFile);

bool debug;

bool isUserInput;
int userNumProcesses;

class Process {
	public:
		int pid;
		int burst;
		int arrival;
		int priority;
		int deadline;
		int io;
		int remaining;
		int finished;
		
		Process();
		
		Process(int pid, int burst, int arrival, int priority, int deadline, int io, int remaining, int finished) {
			this->pid = pid;
			this->burst = burst;
			this->arrival = arrival;
			this->priority = priority;
			this->deadline = deadline;
			this->io = io;
			this->remaining = remaining;
			this->finished = finished;
		}
};

vector<Process*> userVect;
void realTimeUser();

int main(){
	
	cout << "Enter your own processes or read from file?" << endl;
	cout << "0: Enter Processes" << endl;
	cout << "1: Read From File" << endl;
	int number;
	int i = 0;
	int pid;
	int burst;
	int arrival;
	int priority;
	int deadline;
	int io;
	cin >> number;
	if(number == 0){
		isUserInput = true;
		//string continu = "y";
		cout << "Enter the total number of processes: " << endl;
		cin >> userNumProcesses;
		while(i < userNumProcesses){
			cout << "Enter pid for process " << i << ": " << endl;
			cin >> pid;
			cout << "Enter burst for process " << i << ": " << endl;
			cin >> burst;
			cout << "Enter arrival for process " << i << ": " << endl;
			cin >> arrival;
			cout << "Enter priority for process " << i << ": " << endl;
			cin >> priority;
			cout << "Enter deadline for process " << i << ": " << endl;
			cin >> deadline;
			cout << "Enter io for process " << i << ": " << endl;
			cin >> io;
			//cout << "Would you like to enter another process? y/n" << endl;
			//cin >> continu;

			Process* p = new Process(pid, burst, arrival, priority, deadline, io, 0 ,0);
			userVect.push_back(p);

			pid = 0;
			burst = 0;
			arrival = 0;
			priority = 0;
			deadline = 0;
			io = 0;

			i++;
		}
	}
	ifstream inFile;
	if (number == 1) {
		//File name
		string fname = "";

		//Enter file name to test EX: "test"
		cout << "What file do you want to test?" << endl;
		cin >> fname;
		
		//inFile = new ifstream();
		inFile.open(fname.c_str());

		//If file doesn't exist in directory
		if (!inFile) {
			cerr << "Can't open file " << fname << endl;
			exit(1);
		}
	}
	
	//Pick which algorithm
	string alg;
	cout << "Would you like to use MFQS, RTS, or WHS?" << endl;
	cin >> alg;
	
	//Debugger on or off
	string debugAns;
	cout << "Would you like to debug? y/n" << endl;
	cin >> debugAns;
	
	if(debugAns == "y" || debugAns == "Y"){
		debug = true;
	}else{
		debug = false;
	}
	
	//Check for bad input
	if(!(isValidAlg(alg))){
		cout << "Please enter a valid algorithm" << endl;
	}
	
	//Print full name of algorithm... this is where we call the methods????
	if(alg == "MFQS" || alg == "mfqs"){
		cout << "Multilevel Feedback Queue Scheduler" << endl;
		if (number == 1) {
			multilevel(inFile);
		}
	}else if(alg == "RTS" || alg == "rts"){
		cout << "Real Time Scheduler" << endl;
		if (number == 1) {
			realTime(inFile);
		}
		else if(number == 0){
			realTimeUser();
		}
	}else if(alg == "WHS" || alg == "whs"){
		cout << "Windows Hybrid Scheduler" << endl;
		windows(inFile);
	}
	
	//Close file stream
	inFile.close();
	
	return 0;
}

/**
	Pass by reference ifstream
	Multilevel Feedback Queue Scheduling Algorithm
**/
void multilevel(ifstream& inFile){
	
}

/**
	Pass by reference ifstream
	Real Time Scheduling Algorithm
**/
void realTime(ifstream& inFile){
	//Testing reading and printing from the file
	string line;
	string first;
	istringstream stream;
	int vars[6];
	int i;
	
	vector<Process*> processes;
	
	if(debug){
		cout << "Debug ON" << endl;
	}else{
		cout << "Debug OFF" << endl;
	}
	
	//Get first line and do nothing with it
	getline(inFile, first);
	i=0;
	while(getline(inFile, line)){
		stream.str(line);
		stream >> vars[0] >> vars[1] >> vars[2] >> vars[3] >> vars[4] >> vars[5];
		
		if(vars[0] <= 0 || vars[1] <= 0 || vars[2] < 0 || vars[3] < 0 || vars[4] <= 0 || vars[5] < 0){
			if(debug){
				//cout << "Process " << vars[0] << " will not run" << endl;
			}
			i++;
			stream.clear();
			continue;
		}
		
		Process* p = new Process(vars[0], vars[1], vars[2], vars[3], vars[4], vars[5], vars[1], 0);
		processes.push_back(p);
		
		stream.clear();	
		i++;
	}
	
	sort(processes.begin(), processes.end(),
		[](Process* a, Process* b) -> bool
		{
			if(a->arrival == b->arrival){
				return a->deadline < b->deadline;
			}else{
				return a->arrival < b->arrival;
			}
		});
		
	deque<Process*> running;
	vector<Process*>::iterator it;
	int time = processes.front()->arrival;
	i=0;
	int j;
	int numFinished = 0;
	double localATT;
	double totalATT;
	double totalAWT;

	while(i < processes.size() || running.front()->remaining > 0){
		j=0;
		for(j=0; j < processes.size(); j++){
			if(processes[j]->deadline < processes[j]->remaining + time){
				if(debug){
					//cout << "Process " << processes[j]->pid << " will not run " << endl;
				}
				processes.erase(processes.begin()+j);
			}else if(time == processes[j]-> arrival){
				running.push_front(processes[j]);
				processes.erase(processes.begin()+j);
			}
		}
		j=0;
		for(j=0; j < running.size(); j++){
			if(running[j]->deadline < running[j]->remaining + time){
				if(debug){
					//cout << "Process " << running[j]->pid << " will not run " << endl;
				}
				running.erase(running.begin()+j);
			}
		}
		
		sort(running.begin(), running.end(),
			[](Process* a, Process* b) -> bool
			{
				if(a->deadline == b->deadline){
					return a->arrival < b->arrival;
				}else{
					return a->deadline < b->deadline;
				}
				
			});
		if(running.front()->burst == running.front()->remaining){
			if(debug){
				//cout << "Process " << running.front()->pid << " ran at " << time << endl;
			}
		}
		
		running.front()->remaining--;

		if(running.front()->remaining == 0){
			if(debug){
				cout << "Process " << running.front()->pid << " finished at " << time << " PID:" << running.front()->pid << " Burst:" << running.front()->burst << " Arrival:" << running.front()->arrival << " Deadline:" << running.front()->deadline << endl;
				cout << endl;
			}
			localATT = time - running.front()->arrival;
			totalATT = totalATT + localATT;
			totalAWT = totalAWT + (localATT - running.front()->burst);
			running.front()->finished = time;
			running.pop_front();
			numFinished++;
		}
		
		time++;
		i++;
	}
	cout << endl;
	double awt = totalAWT / numFinished;
	double att = totalATT / numFinished;
	cout << "Average Turnaround Time: " << awt << endl;
	cout << "Average Waiting Time: " << att << endl;
	cout << "Number of processes finished: " << numFinished << endl;
}

void realTimeUser() {

	if (debug) {
		cout << "Debug ON" << endl;
	}
	else {
		cout << "Debug OFF" << endl;
	}

	sort(userVect.begin(), userVect.end(),
		[](Process* a, Process* b) -> bool
	{
		if (a->arrival == b->arrival) {
			return a->deadline < b->deadline;
		}
		else {
			return a->arrival < b->arrival;
		}
	});

	deque<Process*> running;
	vector<Process*>::iterator it;
	int time = userVect.front()->arrival;
	int i = 0;
	int j;
	int numFinished = 0;
	double localATT;
	double totalATT;
	double totalAWT;

	while (i < userVect.size() || userVect.front()->remaining > 0) {
		j = 0;
		for (j = 0; j < userVect.size(); j++) {
			if (userVect[j]->deadline < userVect[j]->remaining + time) {
				if (debug) {
					cout << "Process " << userVect[j]->pid << " will not run " << endl;
				}
				userVect.erase(userVect.begin() + j);
			}
			else if (time == userVect[j]->arrival) {
				running.push_front(userVect[j]);
				userVect.erase(userVect.begin() + j);
			}
		}
		j = 0;
		for (j = 0; j < running.size(); j++) {
			if (running[j]->deadline < running[j]->remaining + time) {
				if (debug) {
					cout << "Process " << running[j]->pid << " will not run " << endl;
				}
				running.erase(running.begin() + j);
			}
		}

		sort(running.begin(), running.end(),
			[](Process* a, Process* b) -> bool
		{
			if (a->deadline == b->deadline) {
				return a->arrival < b->arrival;
			}
			else {
				return a->deadline < b->deadline;
			}

		});
		if (running.front()->burst == running.front()->remaining) {
			if (debug) {
				cout << "Process " << running.front()->pid << " ran at " << time << endl;
			}
		}

		running.front()->remaining--;

		if (running.front()->remaining == 0) {
			if (debug) {
				cout << "Process " << running.front()->pid << " finished at " << time << endl;
			}
			localATT = time - running.front()->arrival;
			totalATT = totalATT + localATT;
			totalAWT = totalAWT + (localATT - running.front()->burst);
			running.front()->finished = time;
			running.pop_front();
			numFinished++;
		}

		time++;
		i++;
	}
	cout << endl;
	double awt = totalAWT / numFinished;
	double att = totalATT / numFinished;
	cout << "Average Turnaround Time: " << awt << endl;
	cout << "Average Waiting Time: " << att << endl;
	cout << "Number of processes finished: " << numFinished << endl;
}

/**
	Pass by reference ifstream
	Windows Hybrid Scheduling Algorithm
**/
void windows(ifstream& inFile){
	
}

/**
	Pass in algorithm argument
	Return true if algorithm input is valid
**/
bool isValidAlg(string alg){
	return alg == "MFQS" || alg == "mfqs" || alg == "RTS" || alg == "rts" || alg == "WHS" || alg == "whs";
}
